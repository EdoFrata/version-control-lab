#include <iostream>
#include <string>

std::string phone;

int main()
{
  std::cout << "Insert the phone number: :";

  std::cin >> phone;

  std::cout << "The formatted phone number is: " << "(" <<  phone.substr(0,3) << ")" << " " <<
    phone.substr(3,4) << " " << phone.substr(7,4);

  return 0;
 
}
