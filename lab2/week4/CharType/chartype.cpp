#include <iostream>
#include <string>
#include <cctype>

using namespace std;

char value;

int main()
{

 cout << ("Please enter a character: ");

  cin >> value;
  
  if(value == 'a' || value == 'e' || value == 'i' || value == 'o' || value == 'u') {

    cout << ("(") << value << (")") << (" is a Vowel");
  }

  else if(value >= 'a' && value <= 'z') {

    cout << ("(") << value << (")") << (" is a consonant");

  }

  else if(value >= '0' && value <= '9') {

    cout << ("(") << value << (")") << (" is a digit");

  }
  else if(ispunct(value)) {

    cout << ("(") << value << (")") << (" is a punctuation");
  }  else{
    cout << ("(") << value << (")") << (" is unrecognised");
  }
}
